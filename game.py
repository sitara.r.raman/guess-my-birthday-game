from random import randint

cont = 0

while cont == 0:
    bday_month = randint(1,12)
    bday_year = randint(1924, 2004)

    user_month = int(input("Enter the birthday month"))
    user_year = int(input("Enter the birth year"))

    if (user_month == bday_month) and (user_year == bday_year):
        print("You are correct")
    elif (user_month == bday_month) and (user_year != bday_year):
        print("The month is correct but the year is wrong. The birth year is", bday_year)
    elif (user_month != bday_month) and (user_year == bday_year):
        print("The year is correct but the month is wrong. The birthday month is", bday_month)
    else:
        print("You are wrong, the birthday month is", bday_month, "and the birthday year is", bday_year)

    cont = int(input("Do you want to guess again? Press any number to exit"))
    if cont != 0:
        exit()
